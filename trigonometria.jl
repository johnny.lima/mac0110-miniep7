
# MAC0110 - MiniEP7
# Johnny da Silva Lima - 11796955

# Parte 1
# Função seno
function sin(x)
    x0 = 0
    x = x * (BigFloat(pi / 180))
    
    for i in 0:11
        sinl = (-1)^i
        k = 2 * i + 1
        num = sinl * ((x ^ k) / (factorial(BigInt(k))))
        atual = x0 + num
        x0 = atual
        end
    return x0
    end

# Função coseno
function cos(x)
    x0 = 1
    x = x * (BigFloat(pi / 180))
    for i in 0:11
        sinl = (-1)^(i+1)
        k = 2 * i + 2
        num = sinl * ((x ^ k) / (factorial(BigInt(k))))
        atual = x0 + num
        x0 = atual
        end
    return x0
    end
    
# Função tangente
function B(n)
    if n == 1
        return 1//6
    end
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function tan(x)
    x = x * (pi/180)
  x0 = x
 for i in 2:12
y = (2^(2*i) * (2^(2*i) - 1)* B(i) * x ^ (2*i - 1)) / ((factorial(big(2*i))))
x0 = x0 + y
end
 return x0
end

# Parte 2
# Check_sin

function quaseigual(v1, v2)
    erro = 0.0001
    igual = abs(v1 - v2)
    if igual <= erro
        return true
    else
        return false
    end
end

function check_sin(value, x)
    x = x * (180/pi)
    if quaseigual(sin(x), value) 
        return true
    else
        return false
    end
end

# Check_cos

function check_cos(value, x)
    x = x * (180/pi)
    if quaseigual(cos(x), value) 
        return true
    else
        return false
    end
end

# Check tan

function check_tan(value, x)
    x = x * (180/pi)
    if quaseigual(tan(x), value) 
        return true
    else
        return false
    end
end

# Taylor sin

function taylor_sin(x)
    x0 = 0
    x = x * (BigFloat(pi / 180))

    for i in 0:11
        sinl = (-1)^i
        k = 2 * i + 1
        num = sinl * ((x ^ k) / (factorial(BigInt(k))))
        atual = x0 + num
        x0 = atual
        end
    return x0
    end

# Taylor cos

function taylor_cos(x)
    x0 = 1
    x = x * (BigFloat(pi / 180))
    for i in 0:11
        sinl = (-1)^(i+1)
        k = 2 * i + 2
        num = sinl * ((x ^ k) / (factorial(BigInt(k))))
        atual = x0 + num
        x0 = atual
        end
    return x0
    end

# Taylor tan

function taylor_tan(x)
    x = x * (pi/180)
    x0 = x
    for i in 2:12
    y = (2^(2*i) * (2^(2*i) - 1)* B(i) * x ^ (2*i - 1)) / ((factorial(big(2*i))))
    x0 = x0 + y
    end
    return x0
    end

# Parte 3

#Testes automatizados

function test()
    if check_sin(0, 0) != true || check_sin(0.5, pi/6) != true || check_sin(sqrt(3), (2*pi)/3) != false
        return "Erro. Verifique a função sin"
    end
    if check_cos(0.5, pi / 3) != true || check_cos(42, pi) != false || check_cos(0, pi/4) != false
        return "Erro. Verifique a função cos"
    end
    if check_tan(42, pi / 4) != false || check_tan(1, pi/4) != true || check_tan(1.732, pi/3) != true
        return "Erro. Verifique a função tg"
    end
    return "Tudo OK ! ! !"
end

 test()
